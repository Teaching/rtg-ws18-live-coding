#include "Terrain.hh"

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include "vertex.hh"

using namespace glow;

void Terrain::init()
{
    // load texture
    mTexture = Texture2D::createFromFile(util::pathOf(__FILE__) + "/../textures/TexturesCom_Rock_CliffDesert_1K_albedo.png", ColorSpace::sRGB);

    // load shader
    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/mesh");

    // create mesh
    std::vector<vertex> vertices;
    std::vector<int> indices;


    mHeights.resize(numberOfGridPoints());
    vertices.resize(numberOfGridPoints());

    for (auto y = 0; y < mSize; ++y)
        for (auto x = 0; x < mSize; ++x)
        {
            auto h = glm::sin(x / 4.124f) * glm::sin(y / 5.31f);

            mHeights[idxOf(x, y)] = h * 8;
        }

    for (auto y = 0; y < mSize; ++y)
        for (auto x = 0; x < mSize; ++x)
        {
            auto& v = vertices[idxOf(x, y)];
            v.position = posOf(x, y);
            v.normal = {0,1,0};
        }

    for (auto y = 0; y < mSize - 1; ++y)
        for (auto x = 0; x < mSize - 1; ++x)
        {
            // 00 --- 10
            //  |      |
            // 01 --- 11
            auto i00 = idxOf(x + 0, y + 0);
            auto i01 = idxOf(x + 0, y + 1);
            auto i10 = idxOf(x + 1, y + 0);
            auto i11 = idxOf(x + 1, y + 1);

            indices.push_back(i00);
            indices.push_back(i01);
            indices.push_back(i11);

            indices.push_back(i00);
            indices.push_back(i11);
            indices.push_back(i10);

            auto p00 = vertices[i00].position;
            auto p01 = vertices[i01].position;
            auto p10 = vertices[i10].position;
            auto p11 = vertices[i11].position;

            auto n0 = cross(p01 - p00, p11 - p00);
            auto n1 = cross(p11 - p00, p10 - p00);

            auto n = n0 + n1;

            vertices[i00].normal += n;
            vertices[i01].normal += n;
            vertices[i10].normal += n;
            vertices[i11].normal += n;
        }

    for (auto& v : vertices)
        v.normal = normalize(v.normal);

    auto ab = ArrayBuffer::create({{&vertex::position, "aPosition"}, //
                                   {&vertex::normal, "aNormal"}});
    ab->bind().setData(vertices);

    auto eab = ElementArrayBuffer::create(indices);

    mMesh = VertexArray::create(ab, eab, GL_TRIANGLES);
}

void Terrain::render(const glow::camera::GenericCamera &cam)
{
    GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

    auto shader = mShader->use();

    shader.setUniform("uProj", cam.getProjectionMatrix());
    shader.setUniform("uView", cam.getViewMatrix());
    shader.setTexture("uTexture", mTexture);

    mMesh->bind().draw();
}
