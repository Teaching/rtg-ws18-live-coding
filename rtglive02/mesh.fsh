uniform sampler2D uTexture;

in vec3 vPosition;
in vec3 vNormal;

out vec3 fColor;

void main()
{
    vec2 uv = vPosition.xz;
    vec3 color = texture(uTexture, uv).rgb;

    color = pow(color, vec3(1 / 2.2));
    fColor = color * max(vNormal.y, 0.0);
}
