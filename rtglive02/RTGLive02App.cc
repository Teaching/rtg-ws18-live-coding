#include "RTGLive02App.hh"

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

using namespace glow;

void RTGLive02App::init()
{
    GlfwApp::init();

    setTitle("RTG Live Coding");

    // bg
    {
        auto basepath = util::pathOf(__FILE__) + "/../textures/beach/";
        mQuad = geometry::Quad<>().generate();
        mShaderBG = Program::createFromFile(util::pathOf(__FILE__) + "/bg");
        mSkybox = TextureCubeMap::createFromData(                  //
            TextureData::createFromFileCube(basepath + "posx.jpg", //
                                            basepath + "negx.jpg", //
                                            basepath + "posy.jpg", //
                                            basepath + "negy.jpg", //
                                            basepath + "posz.jpg", //
                                            basepath + "negz.jpg", //
                                            ColorSpace::sRGB));
    }

    mTerrain.init();

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Terrain");
        nanoForm()->addVariable("Wireframe", mTerrain.mWireframe);
    }
}

void RTGLive02App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    // update ui
    nanoForm()->refresh();
}

void RTGLive02App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    auto cam = getCamera();

    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);

    GLOW_SCOPED(clearColor, glm::vec3(1, 1, 1));
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // bg
    {
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = mShaderBG->use();
        shader.setTexture("uSkybox", mSkybox);
        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
        mQuad->bind().draw();
    }

    mTerrain.render(*cam);
}
