#pragma once

#include <random>
#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include "vertex.hh"
#include "Terrain.hh"

class RTGLive02App : public glow::glfw::GlfwApp
{
    // logic
private:
    Terrain mTerrain;

    // rendering
private:
    glow::SharedVertexArray mQuad;
    glow::SharedProgram mShaderBG;
    glow::SharedTextureCubeMap mSkybox;

public:
    /// Called once GLOW is initialized. Allocated your resources and init your logic here.
    virtual void init() override;
    /// Called with at 1 / getUpdateRate() Hz (timestep)
    virtual void update(float elapsedSeconds) override;
    /// Called as fast as possible for rendering (elapsedSeconds is not fixed here)
    virtual void render(float elapsedSeconds) override;
};
