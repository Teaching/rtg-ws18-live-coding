uniform mat4 uProj;
uniform mat4 uView;

in vec3 aPosition;
in vec3 aNormal;

out vec3 vPosition;
out vec3 vNormal;

void main()
{
    vPosition = aPosition;
    vNormal = aNormal;

    gl_Position = uProj * uView * vec4(aPosition, 1);
}
