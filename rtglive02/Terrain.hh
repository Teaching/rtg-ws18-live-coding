#pragma once

#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/camera/GenericCamera.hh>

class Terrain
{
public:
    bool mWireframe = false;

    int mSize = 64;
    std::vector<float> mHeights;

    int numberOfGridPoints() const { return mSize * mSize; }
    int idxOf(int x, int y) const { return y * mSize + x; }
    glm::vec3 posOf(int x, int y) const { return glm::vec3(x - mSize / 2, mHeights[idxOf(x, y)], y - mSize / 2); }

public:
    void init();

    void render(glow::camera::GenericCamera const& cam);

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mMesh;
    glow::SharedTexture2D mTexture;
};
