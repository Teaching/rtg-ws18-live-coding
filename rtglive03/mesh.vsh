uniform mat4 uProj;
uniform mat4 uView;

uniform int uCountX;
uniform int uCountZ;
uniform float uTerrainSize;
uniform vec3 uTerrainOffset;

in vec3 aPosition;
in vec3 aNormal;

out vec3 vPosition;
out vec3 vNormal;

void main()
{
    int id = gl_InstanceID;
    int x = id % uCountX - uCountX / 2;
    int z = id / uCountX - uCountZ / 2;
    vec3 offset = vec3(x, 0, z) * uTerrainSize + uTerrainOffset;

    vPosition = aPosition + offset;
    vNormal = aNormal;

    gl_Position = uProj * uView * vec4(vPosition, 1);
}
