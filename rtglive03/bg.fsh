uniform samplerCube uSkybox;
uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec2 vPosition;

out vec3 fColor;

void main()
{
    vec4 viewNear = uInvProj * vec4(vPosition * 2 - 1, 0, 1);
    vec4 viewFar = uInvProj * vec4(vPosition * 2 - 1, 1, 1);
    viewNear /= viewNear.w;
    viewFar /= viewFar.w;
    vec4 worldNear = uInvView * viewNear;
    vec4 worldFar = uInvView * viewFar;
    vec3 dir = worldFar.xyz - worldNear.xyz;

    vec3 color = texture(uSkybox, dir).rgb;

    color = pow(color, vec3(1 / 2.2));
    fColor = color;
}
