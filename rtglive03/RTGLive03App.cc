#include "RTGLive03App.hh"

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

#include <GLFW/glfw3.h>

using namespace glow;

void RTGLive03App::rebuildTerrain()
{
    mTerrain.rebuild();
}

bool RTGLive03App::onKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
        switch (key)
        {
        case GLFW_KEY_R:
            rebuildTerrain();
            return true;

        case GLFW_KEY_C:
            mEnableCharacter = !mEnableCharacter;
            return true;
        }

    if (mEnableCharacter)
        if (mCharacter.onKey(key, scancode, action, mods))
            return true;

    return GlfwApp::onKey(key, scancode, action, mods);
}

bool RTGLive03App::onMousePosition(double x, double y)
{
    if (mEnableCharacter)
        if (mCharacter.onMousePosition(x, y))
            return true;

    return GlfwApp::onMousePosition(x, y);
}

void RTGLive03App::init()
{
    GlfwApp::init();

    setTitle("RTG Live Coding");

    // bg
    {
        auto basepath = util::pathOf(__FILE__) + "/../textures/beach/";
        mQuad = geometry::Quad<>().generate();
        mShaderBG = Program::createFromFile(util::pathOf(__FILE__) + "/bg");
        mSkybox = TextureCubeMap::createFromData(                  //
            TextureData::createFromFileCube(basepath + "posx.jpg", //
                                            basepath + "negx.jpg", //
                                            basepath + "posy.jpg", //
                                            basepath + "negy.jpg", //
                                            basepath + "posz.jpg", //
                                            basepath + "negz.jpg", //
                                            ColorSpace::sRGB));
    }

    mTerrain.init();

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Terrain");
        nanoForm()->addButton("[R]ebuild", [this] { rebuildTerrain(); });
        nanoForm()->addVariable("Wireframe", mTerrain.mWireframe);
        nanoForm()->addVariable("Amplitude", mTerrain.mAmplitude);
        nanoForm()->addVariable("Scale Factor", mTerrain.mScaleFactor);
        nanoForm()->addVariable("Seed", mTerrain.mSeed);
        nanoForm()->addVariable("# X", mTerrain.mCountX);
        nanoForm()->addVariable("# Z", mTerrain.mCountZ);
    }
}

void RTGLive03App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    setCursorMode(mEnableCharacter ? glfw::CursorMode::Disabled : glfw::CursorMode::Normal);

    if (mEnableCharacter)
        mCharacter.update(elapsedSeconds, *getCamera(), mTerrain);

    // update ui
    nanoForm()->refresh();
}

void RTGLive03App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    auto cam = getCamera();

    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);

    GLOW_SCOPED(clearColor, glm::vec3(1, 1, 1));
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // bg
    {
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = mShaderBG->use();
        shader.setTexture("uSkybox", mSkybox);
        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
        mQuad->bind().draw();
    }

    mTerrain.render(*cam);
}
