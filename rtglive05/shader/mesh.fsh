uniform sampler2D uTexture0Albedo;
uniform sampler2D uTexture0AO;
uniform sampler2D uTexture0Height;
uniform sampler2D uTexture0Normal;
uniform sampler2D uTexture0Roughness;

uniform sampler2D uTexture1Albedo;
uniform sampler2D uTexture1AO;
uniform sampler2D uTexture1Height;
uniform sampler2D uTexture1Normal;
uniform sampler2D uTexture1Roughness;

uniform sampler2D uTexture2Albedo;
uniform sampler2D uTexture2AO;
uniform sampler2D uTexture2Height;
uniform sampler2D uTexture2Normal;
uniform sampler2D uTexture2Roughness;

uniform vec3 uSunColor;
uniform vec3 uAmbientColor;
uniform vec3 uSunDir;
uniform vec3 uCameraPos;

in vec3 vPosition;
in vec3 vTangent;
in vec3 vBitangent;
in vec3 vSplat;
in float vAO;

out vec3 fColor;

// DO NOT MULTIPLY BY COS THETA
vec3 shadingSpecularGGX(vec3 N, vec3 V, vec3 L, float roughness, vec3 F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (denom * denom);
    // no pi because BRDF -> lighting

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    vec3 F = mix(vec3(F_b), vec3(F_a), F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization

    // '/ dotLN' - canceled by lambert
    // '/ dotNV' - canceled by G
    return D * F * G / 4.0;
}

// specular and diffuse contribution of a single light direction
// DOES NOT INCLUDE IBL
vec3 shadingGGX(vec3 N, vec3 V, vec3 L, vec3 color, float roughness, float metallic)
{
    vec3 diffuse = color * (1 - metallic); // metals have no diffuse
    vec3 specular = mix(vec3(0.04), color, metallic); // fixed spec for non-metals

    float dotNL = max(dot(N, L), 0.0);

    return diffuse * dotNL + shadingSpecularGGX(N, V, L, max(0.01, roughness), specular);
}

vec4 texSplatHard(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float smax = max(splat.x, max(splat.y, splat.z));
    if (splat.x == smax)
        return texture(tex0, uv);
    if (splat.y == smax)
        return texture(tex1, uv);
    return texture(tex2, uv);
}

vec4 texSplatExp(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float eps = 0.02;
    float smax = max(splat.x, max(splat.y, splat.z));

    vec4 t0 = texture(tex0, uv);
    vec4 t1 = texture(tex1, uv);
    vec4 t2 = texture(tex2, uv);

    float w0 = smoothstep(smax - eps, smax, splat.x);
    float w1 = smoothstep(smax - eps, smax, splat.y);
    float w2 = smoothstep(smax - eps, smax, splat.z);

    return (t0 * w0 + t1 * w1 + t2 * w2) / (w0 + w1 + w2);
}

vec4 texSplat(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float eps = 0.02;
    float smax = max(splat.x, max(splat.y, splat.z));

    vec4 res = vec4(0);
    float ws = 0;

    if (splat.x > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.x);
        res += w * texture(tex0, uv);
        ws += w;
    }

    if (splat.y > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.y);
        res += w * texture(tex1, uv);
        ws += w;
    }

    if (splat.z > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.z);
        res += w * texture(tex2, uv);
        ws += w;
    }
    
    return res / ws;
}

void main()
{
    float scale = 4;
    float eps = 0.001;
    float apparent_height = 0.1;

    vec2 uv = vPosition.xz / scale;

    // directions
    vec3 T = normalize(vTangent);
    vec3 B = normalize(vBitangent);
    vec3 N = normalize(cross(T, B));
    vec3 L = normalize(uSunDir);
    vec3 V = normalize(uCameraPos - vPosition);

    // material parameters
    vec3 splat = vSplat;
    splat.x += texture(uTexture0Height, uv).x;
    splat.y += texture(uTexture1Height, uv).x;
    splat.z += texture(uTexture2Height, uv).x;
    vec3 albedo = texSplat(splat, uv, uTexture0Albedo, uTexture1Albedo, uTexture2Albedo).rgb;
    vec3 nmap = texSplat(splat, uv, uTexture0Normal, uTexture1Normal, uTexture2Normal).rgb;
    float roughness = texSplat(splat, uv, uTexture0Roughness, uTexture1Roughness, uTexture2Roughness).r;
    float ao = texSplat(splat, uv, uTexture0AO, uTexture1AO, uTexture2AO).x;
    float metallic = 0;
    
    // normal mapping
    nmap.xy = nmap.xy * 2 - 1;
    N = normalize(mat3(T, B, N) * nmap);

    // shading
    fColor = uAmbientColor * albedo;
    fColor += shadingGGX(N, V, L, albedo, roughness, metallic);
    
    // ambient occlusion
    fColor *= min(vAO, ao);
}
