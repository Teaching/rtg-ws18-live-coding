uniform sampler2DRect uTexture;

out vec3 fColor;

void main()
{
    vec3 color = texture(uTexture, gl_FragCoord.xy).rgb;

    // convert to sRGB
    color = pow(color, vec3(1 / 2.224));

    fColor = color;
}