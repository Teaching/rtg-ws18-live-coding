#pragma once

#include <glm/glm.hpp>

struct vertex
{
    glm::vec3 position;
    glm::vec3 tangent;
    glm::vec3 bitangent;
    glm::vec3 splat;
    float ao;
};
