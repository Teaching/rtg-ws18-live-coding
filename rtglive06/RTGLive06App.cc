#include "RTGLive06App.hh"

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

#include <GLFW/glfw3.h>

#include "helper.hh"

using namespace glow;

void RTGLive06App::rebuildTerrain()
{
    mTerrain->rebuild();

    // add lights
    mScene->mPointLights.clear();
    point_light l;
    l.center = glm::vec3(0, 0, 0);
    l.center.y = mTerrain->sampleHeightAtWorldPos(l.center) + 2;
    l.radius = 4;
    l.color = {1, 0, 1};
    mScene->mPointLights.push_back(l);
}

bool RTGLive06App::onKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
        switch (key)
        {
        case GLFW_KEY_R:
            rebuildTerrain();
            return true;

        case GLFW_KEY_C:
            mEnableCharacter = !mEnableCharacter;
            return true;
        }

    if (mEnableCharacter)
        if (mCharacter.onKey(key, scancode, action, mods))
            return true;

    return GlfwApp::onKey(key, scancode, action, mods);
}

bool RTGLive06App::onMousePosition(double x, double y)
{
    if (mEnableCharacter)
        if (mCharacter.onMousePosition(x, y))
            return true;

    return GlfwApp::onMousePosition(x, y);
}

void RTGLive06App::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    for (auto const& target : mTargets)
        target->bind().resize(w, h);
}

void RTGLive06App::init()
{
    GlfwApp::init();

    mTerrain.reset(new Terrain());
    mScene.reset(new Scene());

    glow::DefaultShaderParser::addIncludePath(FILE("shader"));

    setTitle("RTG Live Coding");

    // bg
    {
        auto basepath = FILE("../textures/beach/");
        mQuad = geometry::Quad<>().generate();
        mSkybox = TextureCubeMap::createFromData(                  //
            TextureData::createFromFileCube(basepath + "posx.jpg", //
                                            basepath + "negx.jpg", //
                                            basepath + "posy.jpg", //
                                            basepath + "negy.jpg", //
                                            basepath + "posz.jpg", //
                                            basepath + "negz.jpg", //
                                            ColorSpace::sRGB));
    }

    // pipeline
    {
        // opaque
        mTargets.push_back(mGBufferAlbedo = TextureRectangle::create(1, 1, GL_SRGB8));
        mTargets.push_back(mGBufferNormal = TextureRectangle::create(1, 1, GL_RGB8));
        mTargets.push_back(mGBufferMaterial = TextureRectangle::create(1, 1, GL_RGB8));
        mTargets.push_back(mGBufferDepth = TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32F));
        mFramebufferGBuffer = Framebuffer::create(
            {
                {"fAlbedo", mGBufferAlbedo},     //
                {"fNormal", mGBufferNormal},     //
                {"fMaterial", mGBufferMaterial}, //
            },
            mGBufferDepth);

        // lighting
        mMeshSphere = make_sphere();
        mShaderLightSun = Program::createFromFile("pp.light.sun");
        mShaderLightPoint = Program::createFromFile("light.point");
        mTargets.push_back(mTargetOpaque = TextureRectangle::create(1, 1, GL_RGB16F));
        mFramebufferOpaque = Framebuffer::create("fColor", mTargetOpaque, mGBufferDepth);

        // transparent
        // TODO

        // compositing pass
        mShaderCompositing = Program::createFromFile("pp.compositing");
        mTargets.push_back(mTargetHDR = TextureRectangle::create(1, 1, GL_RGB16F));
        mFramebufferHDR = Framebuffer::create("fColor", mTargetHDR);

        // output
        mShaderOutput = Program::createFromFile("pp.out");
    }

    mTerrain->init();
    rebuildTerrain();

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Terrain");
        nanoForm()->addButton("[R]ebuild", [this] { rebuildTerrain(); });
        nanoForm()->addVariable("Wireframe", mTerrain->mWireframe);
        nanoForm()->addVariable("Amplitude", mTerrain->mAmplitude);
        nanoForm()->addVariable("Scale Factor", mTerrain->mScaleFactor);
        nanoForm()->addVariable("Seed", mTerrain->mSeed);
        nanoForm()->addVariable("# X", mTerrain->mCountX);
        nanoForm()->addVariable("# Z", mTerrain->mCountZ);
    }
}

void RTGLive06App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    setCursorMode(mEnableCharacter ? glfw::CursorMode::Disabled : glfw::CursorMode::Normal);

    if (mEnableCharacter)
        mCharacter.update(elapsedSeconds, *getCamera(), *mTerrain);

    // update ui
    nanoForm()->refresh();
}

void RTGLive06App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    auto cam = getCamera();

    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    // opaque pass
    {
        auto fb = mFramebufferGBuffer->bind();

        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);

        GLOW_SCOPED(enable, GL_FRAMEBUFFER_SRGB);

        // clear
        GLOW_SCOPED(clearColor, glm::vec3(0, 0, 0));
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        mTerrain->render(*cam, *mScene);
    }

    // lighting pass
    {
        auto fb = mFramebufferOpaque->bind();

        GLOW_SCOPED(clearColor, glm::vec3(0, 0, 0));
        glClear(GL_COLOR_BUFFER_BIT);

        // sun
        {
            auto shader = mShaderLightSun->use();

            shader.setUniform("uAmbientColor", mScene->getAmbientColor());
            shader.setUniform("uSunColor", mScene->getSunColor());
            shader.setUniform("uSunDir", mScene->getSunDir());
            shader.setUniform("uCameraPos", cam->getPosition());

            shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
            shader.setUniform("uInvView", inverse(cam->getViewMatrix()));

            shader.setTexture("uTexAlbedo", mGBufferAlbedo);
            shader.setTexture("uTexNormal", mGBufferNormal);
            shader.setTexture("uTexMaterial", mGBufferMaterial);
            shader.setTexture("uTexDepth", mGBufferDepth);

            mQuad->bind().draw();
        }

        // point
        {
            GLOW_SCOPED(enable, GL_CULL_FACE);
            GLOW_SCOPED(enable, GL_DEPTH_TEST);

            GLOW_SCOPED(depthMask, GL_FALSE);   // Disable depth write
            GLOW_SCOPED(cullFace, GL_FRONT);    // Do front-face culling
            GLOW_SCOPED(depthFunc, GL_GREATER); // Inverse z test

            auto shader = mShaderLightPoint->use();
            auto sphere = mMeshSphere->bind();

            shader.setUniform("uProj", cam->getProjectionMatrix());
            shader.setUniform("uView", cam->getViewMatrix());

            for (auto const& l : mScene->mPointLights)
            {
                shader.setUniform("uModel", glm::translate(l.center) * glm::scale(glm::vec3(l.radius * 1.1f)));

                sphere.draw();
            }
        }
    }

    // transparent pass
    {
        // TODO
    }

    // compositing pass
    {
        auto fb = mFramebufferHDR->bind();
        auto shader = mShaderCompositing->use();

        shader.setTexture("uTexOpaque", mTargetOpaque);
        shader.setTexture("uTexDepth", mGBufferDepth);

        shader.setTexture("uSkybox", mSkybox);
        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));

        mQuad->bind().draw();
    }

    // postprocessing
    {
        // TODO
    }

    // write to screen
    {
        auto shader = mShaderOutput->use();

        shader.setTexture("uTexture", mTargetHDR);

        // debug
        shader.setTexture("uTexAlbedo", mGBufferAlbedo);
        shader.setTexture("uTexNormal", mGBufferNormal);
        shader.setTexture("uTexMaterial", mGBufferMaterial);
        shader.setTexture("uTexDepth", mGBufferDepth);
        shader.setTexture("uTexOpaque", mTargetOpaque);

        mQuad->bind().draw();
    }
}
