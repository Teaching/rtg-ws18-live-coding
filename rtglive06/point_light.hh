#pragma once

#include <glm/glm.hpp>

struct point_light
{
    glm::vec3 center;
    glm::vec3 color;
    float radius;
};
