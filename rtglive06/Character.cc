#include "Character.hh"

#include <glm/ext.hpp>

#include <glow/common/log.hh>

#include <glow/gl.hh>

#include <GLFW/glfw3.h>

#include "Terrain.hh"

void Character::update(float elapsedSeconds, glow::camera::GenericCamera& cam, Terrain const& terrain)
{
    // movement
    {
        auto speed = mMoveSpeed * (mPressedShift ? mTurboFactor : 1.0f);

        auto fwd = glm::vec3(glm::cos(mAngleAzimuth), //
                             0,                       //
                             glm::sin(mAngleAzimuth)  //
                             )
                   * speed * elapsedSeconds;

        auto right = cross(fwd, glm::vec3(0, 1, 0));

        mPosition += fwd * float(mPressedForward);
        mPosition -= fwd * float(mPressedBackward);
        mPosition += right * float(mPressedRight);
        mPosition -= right * float(mPressedLeft);

        mPosition.y = terrain.sampleHeightAtWorldPos(mPosition);
    }

    // update camera
    {
        auto fwd = glm::vec3(glm::cos(mAngleAzimuth) * glm::cos(mAngleAltitude), //
                             glm::sin(mAngleAltitude),                           //
                             glm::sin(mAngleAzimuth) * glm::cos(mAngleAltitude)  //
        );

        auto camPos = mPosition + glm::vec3(0, mEyeHeight, 0);
        auto camTarget = camPos + fwd;

        cam.setLookAtMatrix(camPos, camTarget, glm::vec3(0, 1, 0));
    }
}

bool Character::onKey(int key, int scancode, int action, int mods)
{
    switch (key)
    {
    case GLFW_KEY_A:
        if (action == GLFW_PRESS)
        {
            mPressedLeft = true;
            return true;
        }
        if (action == GLFW_RELEASE)
        {
            mPressedLeft = false;
            return true;
        }
        break;
    case GLFW_KEY_D:
        if (action == GLFW_PRESS)
        {
            mPressedRight = true;
            return true;
        }
        if (action == GLFW_RELEASE)
        {
            mPressedRight = false;
            return true;
        }
        break;
    case GLFW_KEY_W:
        if (action == GLFW_PRESS)
        {
            mPressedForward = true;
            return true;
        }
        if (action == GLFW_RELEASE)
        {
            mPressedForward = false;
            return true;
        }
        break;
    case GLFW_KEY_S:
        if (action == GLFW_PRESS)
        {
            mPressedBackward = true;
            return true;
        }
        if (action == GLFW_RELEASE)
        {
            mPressedBackward = false;
            return true;
        }
        break;
    case GLFW_KEY_RIGHT_SHIFT:
    case GLFW_KEY_LEFT_SHIFT:
        if (action == GLFW_PRESS)
        {
            mPressedShift = true;
            return true;
        }
        if (action == GLFW_RELEASE)
        {
            mPressedShift = false;
            return true;
        }
        break;
    }

    return true;
}

bool Character::onMousePosition(double x, double y)
{
    auto dx = x - mLastMouseX;
    auto dy = y - mLastMouseY;

    if (glm::abs(dx) > 100)
        dx = 0;
    if (glm::abs(dy) > 100)
        dy = 0;

    mAngleAzimuth += dx * mLookSpeed;
    mAngleAltitude -= dy * mLookSpeed;

    mAngleAltitude = glm::clamp(mAngleAltitude, glm::radians(-89.99f), glm::radians(89.99f));

    mLastMouseX = x;
    mLastMouseY = y;

    return true;
}
