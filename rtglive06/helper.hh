#pragma once

#include <glow/common/str_utils.hh>

#include <glow/fwd.hh>

#define FILE(name) util::pathOf(__FILE__) + "/" + name

glow::SharedVertexArray make_sphere(int cntU = 32, int cntV = 32);
