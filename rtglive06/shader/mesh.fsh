uniform sampler2D uTexture0Albedo;
uniform sampler2D uTexture0AO;
uniform sampler2D uTexture0Height;
uniform sampler2D uTexture0Normal;
uniform sampler2D uTexture0Roughness;

uniform sampler2D uTexture1Albedo;
uniform sampler2D uTexture1AO;
uniform sampler2D uTexture1Height;
uniform sampler2D uTexture1Normal;
uniform sampler2D uTexture1Roughness;

uniform sampler2D uTexture2Albedo;
uniform sampler2D uTexture2AO;
uniform sampler2D uTexture2Height;
uniform sampler2D uTexture2Normal;
uniform sampler2D uTexture2Roughness;

in vec3 vPosition;
in vec3 vTangent;
in vec3 vBitangent;
in vec3 vSplat;
in float vAO;

out vec3 fAlbedo;
out vec3 fNormal;
out vec3 fMaterial;

vec4 texSplatHard(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float smax = max(splat.x, max(splat.y, splat.z));
    if (splat.x == smax)
        return texture(tex0, uv);
    if (splat.y == smax)
        return texture(tex1, uv);
    return texture(tex2, uv);
}

vec4 texSplatExp(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float eps = 0.02;
    float smax = max(splat.x, max(splat.y, splat.z));

    vec4 t0 = texture(tex0, uv);
    vec4 t1 = texture(tex1, uv);
    vec4 t2 = texture(tex2, uv);

    float w0 = smoothstep(smax - eps, smax, splat.x);
    float w1 = smoothstep(smax - eps, smax, splat.y);
    float w2 = smoothstep(smax - eps, smax, splat.z);

    return (t0 * w0 + t1 * w1 + t2 * w2) / (w0 + w1 + w2);
}

vec4 texSplat(vec3 splat, vec2 uv, sampler2D tex0, sampler2D tex1, sampler2D tex2)
{
    float eps = 0.02;
    float smax = max(splat.x, max(splat.y, splat.z));

    vec4 res = vec4(0);
    float ws = 0;

    if (splat.x > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.x);
        res += w * texture(tex0, uv);
        ws += w;
    }

    if (splat.y > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.y);
        res += w * texture(tex1, uv);
        ws += w;
    }

    if (splat.z > smax - eps)
    {
        float w = smoothstep(smax - eps, smax, splat.z);
        res += w * texture(tex2, uv);
        ws += w;
    }
    
    return res / ws;
}

void main()
{
    float scale = 4;
    float eps = 0.001;
    float apparent_height = 0.1;

    vec2 uv = vPosition.xz / scale;

    // directions
    vec3 T = normalize(vTangent);
    vec3 B = normalize(vBitangent);
    vec3 N = normalize(cross(T, B));

    // material parameters
    vec3 splat = vSplat;
    splat.x += texture(uTexture0Height, uv).x;
    splat.y += texture(uTexture1Height, uv).x;
    splat.z += texture(uTexture2Height, uv).x;
    vec3 albedo = texSplat(splat, uv, uTexture0Albedo, uTexture1Albedo, uTexture2Albedo).rgb;
    vec3 nmap = texSplat(splat, uv, uTexture0Normal, uTexture1Normal, uTexture2Normal).rgb;
    float roughness = texSplat(splat, uv, uTexture0Roughness, uTexture1Roughness, uTexture2Roughness).r;
    float ao = texSplat(splat, uv, uTexture0AO, uTexture1AO, uTexture2AO).x;
    float metallic = 0;
    
    // normal mapping
    nmap.xy = nmap.xy * 2 - 1;
    N = normalize(mat3(T, B, N) * nmap);

    fAlbedo = albedo;
    fNormal = N * .5 + .5;
    fMaterial = vec3(min(vAO, ao), roughness, metallic);
}
