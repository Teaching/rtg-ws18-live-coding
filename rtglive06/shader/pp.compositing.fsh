uniform sampler2DRect uTexOpaque;
uniform sampler2DRect uTexDepth;

uniform samplerCube uSkybox;
uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec2 vPosition;

out vec3 fColor;

void main()
{
    float depth = texture(uTexDepth, gl_FragCoord.xy).x;
    vec3 color;

    if (true || depth < 1) // opaque
    {
        color = texture(uTexOpaque, gl_FragCoord.xy).rgb;
    }
    else // sky
    {
        vec4 viewNear = uInvProj * vec4(vPosition * 2 - 1, 0, 1);
        vec4 viewFar = uInvProj * vec4(vPosition * 2 - 1, 1, 1);
        viewNear /= viewNear.w;
        viewFar /= viewFar.w;
        vec4 worldNear = uInvView * viewNear;
        vec4 worldFar = uInvView * viewFar;
        vec3 dir = worldFar.xyz - worldNear.xyz;

        color = texture(uSkybox, dir).rgb;
    }
    
    // TODO: transparency

    fColor = color;
}
