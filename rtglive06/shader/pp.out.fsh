uniform sampler2DRect uTexture;

uniform sampler2DRect uTexOpaque;
uniform sampler2DRect uTexAlbedo;
uniform sampler2DRect uTexNormal;
uniform sampler2DRect uTexMaterial;
uniform sampler2DRect uTexDepth;

out vec3 fColor;

void main()
{
    vec3 color = texture(uTexture, gl_FragCoord.xy).rgb;

    // convert to sRGB
    color = pow(color, vec3(1 / 2.224));

    fColor = color;
}
