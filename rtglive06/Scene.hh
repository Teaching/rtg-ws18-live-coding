#pragma once

#include <vector>

#include <glm/glm.hpp>

// BEFORE nanogui!
#include <glow/gl.hh>

#include <nanogui/nanogui.h>

#include "point_light.hh"

struct Scene
{
    nanogui::Color ambientColor = {0.2f, 0.2f, 0.2f, 1.0f};
    nanogui::Color sunColor = {1.0f, 1.0f, 1.0f, 1.0f};
    float sunAltitude = 80.0f;
    float sunAzimuth = 0.0f;

    std::vector<point_light> mPointLights;

    glm::vec3 getAmbientColor() const { return {ambientColor.r(), ambientColor.g(), ambientColor.b()}; }
    glm::vec3 getSunColor() const { return {sunColor.r(), sunColor.g(), sunColor.b()}; }

    glm::vec3 getSunDir() const
    {
        float azi = glm::radians(sunAltitude);
        float alt = glm::radians(sunAzimuth);

        return {glm::cos(azi) * glm::cos(alt), //
                glm::sin(alt),                 //
                glm::sin(azi) * glm::cos(alt)};
    }
};
