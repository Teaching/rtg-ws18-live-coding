uniform mat4 uProj;
uniform mat4 uView;

struct flake 
{
    float px, py, pz;
    float vx, vy, vz;
};

layout(std430, binding=0) restrict readonly buffer bSnowBuffer {
    flake snow_flakes[];
};

in vec2 aPosition;

out vec2 vPosition;

void main()
{
    vPosition = aPosition;

    uint fid = gl_InstanceID;
    flake f = snow_flakes[fid];

    vec3 pos = vec3(f.px, f.py, f.pz);

    vec4 cam = uView * vec4(pos, 1);
    cam.xy += aPosition * 0.2;

    gl_Position = uProj * cam;
}