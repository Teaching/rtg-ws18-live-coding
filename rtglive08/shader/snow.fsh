#include "transparency.glsl"

in vec2 vPosition;

void main()
{
    float alpha = 0.7;
    alpha *= smoothstep(0.5, 0.0, distance(vPosition, vec2(0.5)));

    outputTransparent(vec3(1), alpha);
}
