
// output for WBOIT
out vec4 fAccum;
out float fRevealage;

void outputTransparent(vec3 color, float alpha)
{
    // compute weight for WBOIT
    float d = 1 - gl_FragCoord.z;
    float w = alpha * max(1e-2, 3e3 * d * d * d);

    // "write out" accumulation and revealage buffer
    fAccum = vec4(color * alpha * w, alpha * w);
    fRevealage = 1 - alpha;
}
