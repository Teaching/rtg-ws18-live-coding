#include "lighting.glsl"

uniform sampler2DRect uTexAlbedo;
uniform sampler2DRect uTexNormal;
uniform sampler2DRect uTexMaterial;
uniform sampler2DRect uTexDepth;

uniform vec3 uLightColor;
uniform vec3 uLightCenter;
uniform float uLightRadius;

uniform vec3 uCameraPos;

uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec3 vPosition;
in vec4 vNdcPosition;

out vec3 fColor;

void main()
{
    // material parameters
    float depth = texture(uTexDepth, gl_FragCoord.xy).r;
    vec3 albedo = texture(uTexAlbedo, gl_FragCoord.xy).rgb;
    vec3 normal = texture(uTexNormal, gl_FragCoord.xy).rgb;
    vec3 material = texture(uTexMaterial, gl_FragCoord.xy).rgb;
    float ao = material.x;
    float roughness = material.y;
    float metallic = material.z;

    // compute directions
    vec4 ndc = vec4(vNdcPosition.xy / vNdcPosition.w, depth * 2 - 1, 1);
    vec3 N = normalize(normal * 2 - 1);
    vec3 P = unproject(ndc, uInvProj, uInvView);
    vec3 L = normalize(uLightCenter - P);
    vec3 V = normalize(uCameraPos - P);

    // shading
    float attenuation = smoothstep(uLightRadius, 0.0, distance(uLightCenter, P));
    fColor = attenuation * uLightColor * shadingGGX(N, V, L, albedo, roughness, metallic);
}
