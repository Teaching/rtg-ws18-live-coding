#include "lighting.glsl"

uniform sampler2DRect uTexAlbedo;
uniform sampler2DRect uTexNormal;
uniform sampler2DRect uTexMaterial;
uniform sampler2DRect uTexDepth;

uniform vec3 uSunColor;
uniform vec3 uAmbientColor;
uniform vec3 uSunDir;
uniform vec3 uCameraPos;

uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec2 vPosition;

out vec3 fColor;

void main()
{
    float depth = texture(uTexDepth, gl_FragCoord.xy).r;
    if (depth == 1)
        discard;

    // material parameters
    vec3 albedo = texture(uTexAlbedo, gl_FragCoord.xy).rgb;
    vec3 normal = texture(uTexNormal, gl_FragCoord.xy).rgb;
    vec3 material = texture(uTexMaterial, gl_FragCoord.xy).rgb;
    float ao = material.x;
    float roughness = material.y;
    float metallic = material.z;

    // compute directions
    vec4 ndc = vec4(vPosition * 2 - 1, depth * 2 - 1, 1);
    vec3 N = normalize(normal * 2 - 1);
    vec3 P = unproject(ndc, uInvProj, uInvView);
    vec3 L = normalize(uSunDir);
    vec3 V = normalize(uCameraPos - P);

    // shading
    fColor = uAmbientColor * albedo;
    fColor += uSunColor * shadingGGX(N, V, L, albedo, roughness, metallic);
    
    // ambient occlusion
    fColor *= ao;
}
