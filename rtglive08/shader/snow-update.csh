layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

struct flake 
{
    float px, py, pz;
    float vx, vy, vz;
};

layout(std430, binding=0) restrict buffer bSnowBuffer {
    flake snow_flakes[];
};

uniform float uElapsedSeconds;
uniform float uTotalSeconds;

uniform vec3 uCamPos;

void main()
{
    uint fid = gl_GlobalInvocationID.x;
    flake f = snow_flakes[fid];

    vec3 p = vec3(f.px, f.py, f.pz);
    vec3 v = vec3(f.vx, f.vy, f.vz);

    v = vec3(0, -1, 0);
    v.x = sin(uTotalSeconds * 0.9 + 1.42 + float(fid) * 16.21);
    v.z = sin(uTotalSeconds * 1.1 + 6.21 + float(fid) * 9.51 );

    p += v * uElapsedSeconds;

    if (p.x < uCamPos.x - 25)
        p.x += 50;
    if (p.y < uCamPos.y - 25)
        p.y += 50;
    if (p.z < uCamPos.z - 25)
        p.z += 50;

    if (p.x > uCamPos.x + 25)
        p.x -= 50;
    if (p.y > uCamPos.y + 25)
        p.y -= 50;
    if (p.z > uCamPos.z + 25)
        p.z -= 50;

    f.px = p.x;
    f.py = p.y;
    f.pz = p.z;
    f.vx = v.x;
    f.vy = v.y;
    f.vz = v.z;
    snow_flakes[fid] = f;
}