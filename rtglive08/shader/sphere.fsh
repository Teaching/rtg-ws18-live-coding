#include "lighting.glsl"
#include "transparency.glsl"

uniform vec3 uSphereColor;

uniform vec3 uSunColor;
uniform vec3 uAmbientColor;
uniform vec3 uSunDir;
uniform vec3 uCameraPos;

in vec3 vWorldPosition;
in vec3 vNormal;

void main()
{
    float alpha = 0.4;
    vec3 albedo = uSphereColor;
    vec3 N = normalize(vNormal);
    vec3 V = normalize(uCameraPos - vWorldPosition);
    vec3 L = normalize(uSunDir);
    float roughness = 0.5;
    float metallic = 0;

    // fresnel
    alpha = alpha + (1 - alpha) * pow(1 - max(0.0, dot(N, V)), 5);

    vec3 color = uAmbientColor * albedo;
    color += shadingGGX(N, V, L, albedo, roughness, metallic);
    
    outputTransparent(color, alpha);
}