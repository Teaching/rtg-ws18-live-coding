uniform sampler2DRect uTexOpaque;
uniform sampler2DRect uTexDepth;
uniform sampler2DRect uTexAccum;
uniform sampler2DRect uTexRevealage;

uniform samplerCube uSkybox;
uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec2 vPosition;

out vec3 fColor;

void main()
{
    float depth = texture(uTexDepth, gl_FragCoord.xy).x;
    vec3 color;

    if (depth < 1) // opaque
    {
        color = texture(uTexOpaque, gl_FragCoord.xy).rgb;
    }
    else // sky
    {
        vec4 viewNear = uInvProj * vec4(vPosition * 2 - 1, 0, 1);
        vec4 viewFar = uInvProj * vec4(vPosition * 2 - 1, 1, 1);
        viewNear /= viewNear.w;
        viewFar /= viewFar.w;
        vec4 worldNear = uInvView * viewNear;
        vec4 worldFar = uInvView * viewFar;
        vec3 dir = worldFar.xyz - worldNear.xyz;

        color = texture(uSkybox, dir).rgb * 0.1;
    }
    
    // transparency
    {
        vec4 accum = texture(uTexAccum, gl_FragCoord.xy);
        float revealage = texture(uTexRevealage, gl_FragCoord.xy).r;

        vec3 t_color = accum.a == 0 ? vec3(0) : accum.rgb / accum.a;

        color = mix(t_color, color, revealage);
    }

    fColor = color;
}
