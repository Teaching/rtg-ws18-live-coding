uniform mat4 uProj;
uniform mat4 uView;
uniform mat4 uModel;

in vec3 aPosition;

out vec3 vPosition;
out vec4 vNdcPosition;

void main()
{
    vPosition = aPosition;
    vNdcPosition = uProj * uView * uModel * vec4(aPosition, 1);
    gl_Position = vNdcPosition;
}
