#include "RTGLive08App.hh"

#include <random>

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

#include <GLFW/glfw3.h>

#include "helper.hh"

using namespace glow;

void RTGLive08App::rebuildTerrain()
{
    mTerrain->rebuild();

    std::default_random_engine rng;
    std::uniform_real_distribution<> dis_radius(4.0, 15.0);
    std::uniform_real_distribution<> dis_color(0.0, 1.0);
    std::uniform_real_distribution<> dis_pos(-300, 300);

    // add lights
    mScene->mPointLights.clear();
    for (auto i = 0; i < 1000; ++i)
    {
        point_light l;
        l.center = glm::vec3(dis_pos(rng), 0, dis_pos(rng));
        l.center.y = mTerrain->sampleHeightAtWorldPos(l.center) + 2;
        l.radius = dis_radius(rng);
        auto r = dis_color(rng);
        auto g = dis_color(rng);
        auto b = dis_color(rng);
        auto mc = glm::max(r, glm::max(g, b));
        l.color = {r / mc, g / mc, b / mc};
        mScene->mPointLights.push_back(l);
    }
}

bool RTGLive08App::onKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
        switch (key)
        {
        case GLFW_KEY_R:
            rebuildTerrain();
            return true;

        case GLFW_KEY_C:
            mEnableCharacter = !mEnableCharacter;
            return true;
        }

    if (mEnableCharacter)
        if (mCharacter.onKey(key, scancode, action, mods))
            return true;

    return GlfwApp::onKey(key, scancode, action, mods);
}

bool RTGLive08App::onMousePosition(double x, double y)
{
    if (mEnableCharacter)
        if (mCharacter.onMousePosition(x, y))
            return true;

    return GlfwApp::onMousePosition(x, y);
}

void RTGLive08App::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    for (auto const& target : mTargets)
        target->bind().resize(w, h);
}

void RTGLive08App::init()
{
    GlfwApp::init();

    mTerrain.reset(new Terrain());
    mScene.reset(new Scene());

    glow::DefaultShaderParser::addIncludePath(FILE("shader"));

    setTitle("RTG Live Coding");

    // bg
    {
        auto basepath = FILE("../textures/beach/");
        mQuad = geometry::Quad<>().generate();
        mSkybox = TextureCubeMap::createFromData(                  //
            TextureData::createFromFileCube(basepath + "posx.jpg", //
                                            basepath + "negx.jpg", //
                                            basepath + "posy.jpg", //
                                            basepath + "negy.jpg", //
                                            basepath + "posz.jpg", //
                                            basepath + "negz.jpg", //
                                            ColorSpace::sRGB));
    }

    // pipeline
    {
        // opaque
        mTargets.push_back(mGBufferAlbedo = TextureRectangle::create(1, 1, GL_SRGB8));
        mTargets.push_back(mGBufferNormal = TextureRectangle::create(1, 1, GL_RGB8));
        mTargets.push_back(mGBufferMaterial = TextureRectangle::create(1, 1, GL_RGB8));
        mTargets.push_back(mGBufferDepth = TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32F));
        mFramebufferGBuffer = Framebuffer::create(
            {
                {"fAlbedo", mGBufferAlbedo},     //
                {"fNormal", mGBufferNormal},     //
                {"fMaterial", mGBufferMaterial}, //
            },
            mGBufferDepth);

        // lighting
        mMeshSphere = make_sphere();
        mShaderLightSun = Program::createFromFile("pp.light.sun");
        mShaderLightPoint = Program::createFromFile("light.point");
        mTargets.push_back(mTargetOpaque = TextureRectangle::create(1, 1, GL_RGB16F));
        mFramebufferOpaque = Framebuffer::create("fColor", mTargetOpaque, mGBufferDepth);

        // transparent
        mShaderObjSphere = Program::createFromFile("sphere");
        mTargets.push_back(mTargetAccum = TextureRectangle::create(1, 1, GL_RGBA16F));
        mTargets.push_back(mTargetRevealage = TextureRectangle::create(1, 1, GL_R16F));
        mFramebufferTransparent = Framebuffer::create({{"fAccum", mTargetAccum}, {"fRevealage", mTargetRevealage}}, mGBufferDepth);

        // compositing pass
        mShaderCompositing = Program::createFromFile("pp.compositing");
        mTargets.push_back(mTargetHDR = TextureRectangle::create(1, 1, GL_RGB16F));
        mFramebufferHDR = Framebuffer::create("fColor", mTargetHDR);

        // output
        mShaderOutput = Program::createFromFile("pp.out");
    }

    // snow
    {
        struct flake
        {
            glm::vec3 pos;
            glm::vec3 vel;
        };
        // initialize snow flakes randomly
        std::vector<flake> snow_flakes(mFlakeCount);
        std::default_random_engine rng;
        std::uniform_real_distribution<> dis(0.0, 1.0);
        for (auto& f : snow_flakes)
        {
            f.pos.y = dis(rng) * 50;

            f.pos.x = dis(rng) * 50 - 25;
            f.pos.z = dis(rng) * 50 - 25;
        }

        mSnowBuffer = ShaderStorageBuffer::create();
        mSnowBuffer->bind().setData(snow_flakes);

        mShaderSnowDraw = Program::createFromFile("snow");
        mShaderSnowUpdate = Program::createFromFile("snow-update");
    }

    mTerrain->init();
    rebuildTerrain();

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Terrain");
        nanoForm()->addButton("[R]ebuild", [this] { rebuildTerrain(); });
        nanoForm()->addVariable("Wireframe", mTerrain->mWireframe);
        nanoForm()->addVariable("Amplitude", mTerrain->mAmplitude);
        nanoForm()->addVariable("Scale Factor", mTerrain->mScaleFactor);
        nanoForm()->addVariable("Seed", mTerrain->mSeed);
        nanoForm()->addVariable("# X", mTerrain->mCountX);
        nanoForm()->addVariable("# Z", mTerrain->mCountZ);
    }
}

void RTGLive08App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    setCursorMode(mEnableCharacter ? glfw::CursorMode::Disabled : glfw::CursorMode::Normal);

    if (mEnableCharacter)
        mCharacter.update(elapsedSeconds, *getCamera(), *mTerrain);

    for (auto& l : mScene->mPointLights)
    {
        auto r = l.radius;
        auto h = r * 0.4f + r * 0.3f * glm::sin(glfwGetTime() + l.center.x + l.center.z);
        l.center.y = mTerrain->sampleHeightAtWorldPos(l.center) + h;
    }

    // update ui
    nanoForm()->refresh();
}

void RTGLive08App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    auto cam = getCamera();

    // update snow
    {
        static double totalSeconds = 0.0;
        totalSeconds += elapsedSeconds;

        mShaderSnowUpdate->setShaderStorageBuffer("bSnowBuffer", mSnowBuffer);
        auto shader = mShaderSnowUpdate->use();
        shader.setUniform("uElapsedSeconds", elapsedSeconds);
        shader.setUniform("uTotalSeconds", (float)totalSeconds);
        shader.setUniform("uCamPos", cam->getPosition());
        shader.compute(mFlakeCount / 128);
    }

    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    // opaque pass
    {
        auto fb = mFramebufferGBuffer->bind();

        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);

        GLOW_SCOPED(enable, GL_FRAMEBUFFER_SRGB);

        // clear
        GLOW_SCOPED(clearColor, glm::vec3(0, 0, 0));
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        mTerrain->render(*cam, *mScene);
    }

    // lighting pass
    {
        auto fb = mFramebufferOpaque->bind();

        GLOW_SCOPED(clearColor, glm::vec3(0, 0, 0));
        glClear(GL_COLOR_BUFFER_BIT);

        // sun
        {
            auto shader = mShaderLightSun->use();

            shader.setUniform("uAmbientColor", mScene->getAmbientColor());
            shader.setUniform("uSunColor", mScene->getSunColor());
            shader.setUniform("uSunDir", mScene->getSunDir());

            shader.setUniform("uCameraPos", cam->getPosition());

            shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
            shader.setUniform("uInvView", inverse(cam->getViewMatrix()));

            shader.setTexture("uTexAlbedo", mGBufferAlbedo);
            shader.setTexture("uTexNormal", mGBufferNormal);
            shader.setTexture("uTexMaterial", mGBufferMaterial);
            shader.setTexture("uTexDepth", mGBufferDepth);

            mQuad->bind().draw();
        }

        // point
        {
            GLOW_SCOPED(enable, GL_CULL_FACE);
            GLOW_SCOPED(enable, GL_DEPTH_TEST);
            GLOW_SCOPED(enable, GL_BLEND);

            GLOW_SCOPED(depthMask, GL_FALSE);       // Disable depth write
            GLOW_SCOPED(cullFace, GL_FRONT);        // Do front-face culling
            GLOW_SCOPED(depthFunc, GL_GREATER);     // Inverse z test
            GLOW_SCOPED(blendFunc, GL_ONE, GL_ONE); // Additive blending

            auto shader = mShaderLightPoint->use();
            auto sphere = mMeshSphere->bind();

            shader.setUniform("uProj", cam->getProjectionMatrix());
            shader.setUniform("uView", cam->getViewMatrix());

            shader.setUniform("uCameraPos", cam->getPosition());

            shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
            shader.setUniform("uInvView", inverse(cam->getViewMatrix()));

            shader.setTexture("uTexAlbedo", mGBufferAlbedo);
            shader.setTexture("uTexNormal", mGBufferNormal);
            shader.setTexture("uTexMaterial", mGBufferMaterial);
            shader.setTexture("uTexDepth", mGBufferDepth);

            for (auto const& l : mScene->mPointLights)
            {
                shader.setUniform("uModel", glm::translate(l.center) * glm::scale(glm::vec3(l.radius * 1.1f)));

                shader.setUniform("uLightCenter", l.center);
                shader.setUniform("uLightColor", l.color);
                shader.setUniform("uLightRadius", l.radius);

                sphere.draw();
            }
        }
    }

    // transparent pass
    {
        mTargetAccum->clear(glm::vec4(0, 0, 0, 0));
        mTargetRevealage->clear(1.0f);

        auto fb = mFramebufferTransparent->bind();

        GLOW_SCOPED(enable, GL_BLEND);
        GLOW_SCOPED(enable, GL_CULL_FACE);

        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(depthMask, GL_FALSE);

        glBlendFunci(0, GL_ONE, GL_ONE);
        glBlendFunci(1, GL_ZERO, GL_SRC_COLOR);

        {
            auto shader = mShaderObjSphere->use();
            shader.setUniform("uProj", cam->getProjectionMatrix());
            shader.setUniform("uView", cam->getViewMatrix());
            auto mesh = mMeshSphere->bind();

            shader.setUniform("uAmbientColor", mScene->getAmbientColor());
            shader.setUniform("uSunColor", mScene->getSunColor());
            shader.setUniform("uSunDir", mScene->getSunDir());

            shader.setUniform("uCameraPos", cam->getPosition());

            for (auto const& l : mScene->mPointLights)
            {
                shader.setUniform("uModel", translate(l.center));
                shader.setUniform("uSphereColor", l.color);

                mesh.draw();
            }
        }

        // render snow
        {
            GLOW_SCOPED(disable, GL_CULL_FACE);

            // bind SSBO to shader
            mShaderSnowDraw->setShaderStorageBuffer("bSnowBuffer", mSnowBuffer);

            // set up shadow
            auto shader = mShaderSnowDraw->use();
            shader.setUniform("uProj", cam->getProjectionMatrix());
            shader.setUniform("uView", cam->getViewMatrix());

            // render instanced quad
            mQuad->bind().draw(mFlakeCount);
        }
    }

    // compositing pass
    {
        auto fb = mFramebufferHDR->bind();
        auto shader = mShaderCompositing->use();

        shader.setTexture("uTexOpaque", mTargetOpaque);
        shader.setTexture("uTexDepth", mGBufferDepth);
        shader.setTexture("uTexAccum", mTargetAccum);
        shader.setTexture("uTexRevealage", mTargetRevealage);

        shader.setTexture("uSkybox", mSkybox);
        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));

        mQuad->bind().draw();
    }

    // postprocessing
    {
        // TODO
    }

    // write to screen
    {
        auto shader = mShaderOutput->use();

        shader.setTexture("uTexture", mTargetHDR);

        // debug
        shader.setTexture("uTexAlbedo", mGBufferAlbedo);
        shader.setTexture("uTexNormal", mGBufferNormal);
        shader.setTexture("uTexMaterial", mGBufferMaterial);
        shader.setTexture("uTexDepth", mGBufferDepth);
        shader.setTexture("uTexOpaque", mTargetOpaque);

        mQuad->bind().draw();
    }
}
