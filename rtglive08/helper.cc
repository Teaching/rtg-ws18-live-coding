#include "helper.hh"

#include <fstream>

#include <glm/ext.hpp>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/VertexArray.hh>

using namespace glow;

SharedVertexArray make_sphere(int cntU, int cntV)
{
    std::vector<glm::vec3> vertices;

    // uv in 0..1
    auto pos_of = [](float u, float v) -> glm::vec3 {
        auto cu = glm::cos(u * glm::pi<float>() * 2);
        auto su = glm::sin(u * glm::pi<float>() * 2);
        auto cv = glm::cos(v * glm::pi<float>());
        auto sv = glm::sin(v * glm::pi<float>());

        return {cu * sv, //
                cv,      //
                su * sv};
    };

    auto du = 1.0f / cntU;
    auto dv = 1.0f / cntV;
    for (auto y = 0; y < cntV; ++y)
        for (auto x = 0; x < cntU; ++x)
        {
            auto u = x * du;
            auto v = y * dv;

            auto v00 = pos_of(u, v);
            auto v10 = pos_of(u + du, v);
            auto v01 = pos_of(u, v + dv);
            auto v11 = pos_of(u + du, v + dv);

            vertices.push_back(v00);
            vertices.push_back(v10);
            vertices.push_back(v11);

            vertices.push_back(v00);
            vertices.push_back(v11);
            vertices.push_back(v01);
        }

    std::ofstream obj("/tmp/sphere.obj");
    for (auto const& v : vertices)
        obj << "v " << v.x << " " << v.y << " " << v.z << "\n";

    auto ab = ArrayBuffer::create();
    ab->defineAttribute<glm::vec3>("aPosition");
    ab->bind().setData(vertices);

    return VertexArray::create(ab);
}
