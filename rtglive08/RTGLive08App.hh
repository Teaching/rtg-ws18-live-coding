#pragma once

#include <memory>
#include <random>
#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include "Character.hh"
#include "Scene.hh"
#include "Terrain.hh"
#include "vertex.hh"

class RTGLive08App : public glow::glfw::GlfwApp
{
    // logic
private:
    std::unique_ptr<Scene> mScene;
    std::unique_ptr<Terrain> mTerrain;

    bool mEnableCharacter = true;
    Character mCharacter;

    void rebuildTerrain();

    // rendering
private:
    glow::SharedVertexArray mQuad;

    // snow
    int mFlakeCount = 128 * 1000;
    glow::SharedShaderStorageBuffer mSnowBuffer;
    glow::SharedProgram mShaderSnowDraw;
    glow::SharedProgram mShaderSnowUpdate;

    // opaque pass
    glow::SharedTextureRectangle mGBufferAlbedo;
    glow::SharedTextureRectangle mGBufferNormal;
    glow::SharedTextureRectangle mGBufferMaterial;
    glow::SharedTextureRectangle mGBufferDepth;
    glow::SharedFramebuffer mFramebufferGBuffer;

    // lighting pass
    glow::SharedProgram mShaderLightSun;
    glow::SharedProgram mShaderLightPoint;
    glow::SharedVertexArray mMeshSphere;
    glow::SharedTextureRectangle mTargetOpaque;
    glow::SharedFramebuffer mFramebufferOpaque;

    // transparent pass
    glow::SharedProgram mShaderObjSphere;
    glow::SharedTextureRectangle mTargetAccum;
    glow::SharedTextureRectangle mTargetRevealage;
    glow::SharedFramebuffer mFramebufferTransparent;

    // compositing pass
    glow::SharedTextureCubeMap mSkybox;
    glow::SharedProgram mShaderCompositing;
    glow::SharedTextureRectangle mTargetHDR;
    glow::SharedFramebuffer mFramebufferHDR;

    // post-processing
    // TODO

    // output
    glow::SharedProgram mShaderOutput;

    std::vector<glow::SharedTextureRectangle> mTargets;

public:
    virtual bool onKey(int key, int scancode, int action, int mods) override;
    virtual bool onMousePosition(double x, double y) override;

    virtual void onResize(int w, int h) override;

    /// Called once GLOW is initialized. Allocated your resources and init your logic here.
    virtual void init() override;
    /// Called with at 1 / getUpdateRate() Hz (timestep)
    virtual void update(float elapsedSeconds) override;
    /// Called as fast as possible for rendering (elapsedSeconds is not fixed here)
    virtual void render(float elapsedSeconds) override;
};
