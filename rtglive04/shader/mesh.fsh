uniform sampler2D uTextureAlbedo;
uniform sampler2D uTextureAO;
uniform sampler2D uTextureHeight;
uniform sampler2D uTextureNormal;
uniform sampler2D uTextureRoughness;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBitangent;
in float vAO;

out vec3 fColor;

void main()
{
    float scale = 4;
    float eps = 0.001;
    float apparent_height = 0.1;

    vec2 uv = vPosition.xz / scale;

    // directions
    vec3 T = normalize(vTangent);
    vec3 B = normalize(vBitangent);
    vec3 N = normalize(cross(T, B));
    
    // bump mapping
    float h_mu = texture(uTextureHeight, uv - vec2(eps, 0)).x * apparent_height;
    float h_pu = texture(uTextureHeight, uv + vec2(eps, 0)).x * apparent_height;
    float h_mv = texture(uTextureHeight, uv - vec2(0, eps)).x * apparent_height;
    float h_pv = texture(uTextureHeight, uv + vec2(0, eps)).x * apparent_height;

    float dh_du = (h_pu - h_mu) / (2 * eps);
    float dh_dv = (h_pv - h_mv) / (2 * eps);

    N = normalize(N + dh_du * T - dh_dv * B);

    vec3 color = texture(uTextureAlbedo, uv).rgb;

    fColor = color * max(N.y, 0.0);

    // ambient occlusion
    float ao = texture(uTextureAO, uv).x;
    fColor *= min(vAO, ao);
}
