#pragma once

#include <memory>
#include <random>
#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include "vertex.hh"
#include "Terrain.hh"
#include "Character.hh"

class RTGLive04App : public glow::glfw::GlfwApp
{
    // logic
private:
    std::unique_ptr<Terrain> mTerrain;

    bool mEnableCharacter = true;
    Character mCharacter;

    void rebuildTerrain();

    // rendering
private:
    glow::SharedVertexArray mQuad;
    glow::SharedProgram mShaderBG;
    glow::SharedTextureCubeMap mSkybox;

    glow::SharedProgram mShaderOutput;
    glow::SharedTextureRectangle mTargetColor;
    glow::SharedTextureRectangle mTargetDepth;
    glow::SharedFramebuffer mFramebufferScene;

    std::vector<glow::SharedTextureRectangle> mTargets;

public:
    virtual bool onKey(int key, int scancode, int action, int mods) override;
    virtual bool onMousePosition(double x, double y) override;

    virtual void onResize(int w, int h) override;

    /// Called once GLOW is initialized. Allocated your resources and init your logic here.
    virtual void init() override;
    /// Called with at 1 / getUpdateRate() Hz (timestep)
    virtual void update(float elapsedSeconds) override;
    /// Called as fast as possible for rendering (elapsedSeconds is not fixed here)
    virtual void render(float elapsedSeconds) override;
};
