#include "Terrain.hh"

#include <random>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include "helper.hh"
#include "vertex.hh"

using namespace glow;

void Terrain::init()
{
    initOpenGL();

    rebuild();
}

void Terrain::rebuild()
{
    generateHeightmap();

    buildMesh();
}

void Terrain::render(const glow::camera::GenericCamera& cam)
{
    GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

    auto pos = cam.getPosition();
    auto ox = glm::round(pos.x / Size) * Size;
    auto oz = glm::round(pos.z / Size) * Size;

    auto shader = mShader->use();

    shader.setUniform("uProj", cam.getProjectionMatrix());
    shader.setUniform("uView", cam.getViewMatrix());
    shader.setUniform("uCountX", mCountX);
    shader.setUniform("uCountZ", mCountZ);
    shader.setUniform("uTerrainSize", (float)Size);
    shader.setUniform("uTerrainOffset", glm::vec3(ox, 0, oz));

    shader.setTexture("uTextureAlbedo", mTextureAlbedo);
    shader.setTexture("uTextureAO", mTextureAO);
    shader.setTexture("uTextureHeight", mTextureHeight);
    shader.setTexture("uTextureNormal", mTextureNormal);
    shader.setTexture("uTextureRoughness", mTextureRoughness);

    mMesh->bind().draw(mCountX * mCountZ);
}

void Terrain::initOpenGL()
{
    // load texture
    mTextureAlbedo
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_albedo.png"), ColorSpace::Linear);
    mTextureAO = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_ao.png"), ColorSpace::Linear);
    mTextureHeight
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_height.png"), ColorSpace::Linear);
    mTextureNormal
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_normal.png"), ColorSpace::Linear);
    mTextureRoughness
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_roughness.png"), ColorSpace::Linear);

    // load shader
    mShader = Program::createFromFile("mesh");

    mVertexBuffer = ArrayBuffer::create({{&vertex::position, "aPosition"},   //
                                         {&vertex::normal, "aNormal"},       //
                                         {&vertex::tangent, "aTangent"},     //
                                         {&vertex::bitangent, "aBitangent"}, //
                                         {&vertex::ao, "aAO"}});

    mIndexBuffer = ElementArrayBuffer::create();

    mMesh = VertexArray::create(mVertexBuffer, mIndexBuffer, GL_TRIANGLES);
}

void Terrain::generateHeightmap()
{
    mHeights.clear();
    mHeights.resize(numberOfGridPoints());

    std::default_random_engine rng;
    rng.seed(mSeed);

    mHeights[0] = 0.0f;

    auto s = Size / 2;
    auto scale = mAmplitude;
    while (s >= 1)
    {
        std::normal_distribution<float> noise(0.0f, scale);

        // diamond step
        for (auto y = s; y < Size; y += s * 2)
            for (auto x = s; x < Size; x += s * 2)
            {
                auto h00 = heightAt(x - s, y - s);
                auto h10 = heightAt(x + s, y - s);
                auto h01 = heightAt(x - s, y + s);
                auto h11 = heightAt(x + s, y + s);

                auto h = (h00 + h10 + h01 + h11) / 4;
                h += noise(rng);

                heightAt(x, y) = h;
            }

        // square step
        for (auto y = 0; y < Size; y += s)
            for (auto ix = s; ix < Size; ix += s * 2)
            {
                auto x = ix - (y / s) % 2 * s;

                auto h0 = heightAt(x - s, y);
                auto h1 = heightAt(x + s, y);
                auto h2 = heightAt(x, y - s);
                auto h3 = heightAt(x, y + s);

                auto h = (h0 + h1 + h2 + h3) / 4;
                h += noise(rng);

                heightAt(x, y) = h;
            }

        // advance
        s /= 2;
        scale *= mScaleFactor;
    }
}

void Terrain::buildMesh()
{
    mVertices.clear();
    mIndices.clear();

    mVertices.resize((Size + 1) * (Size + 1));
    for (auto y = 0; y <= Size; ++y)
        for (auto x = 0; x <= Size; ++x)
        {
            auto& v = mVertices[vertexIdxOf(x, y)];
            v.position = posOf(x, y);
            v.normal = {0, 1, 0};
        }

    for (auto y = 0; y < Size; ++y)
        for (auto x = 0; x < Size; ++x)
        {
            // 00 --- 10
            //  |      |
            // 01 --- 11
            auto i00 = vertexIdxOf(x + 0, y + 0);
            auto i01 = vertexIdxOf(x + 0, y + 1);
            auto i10 = vertexIdxOf(x + 1, y + 0);
            auto i11 = vertexIdxOf(x + 1, y + 1);

            mIndices.push_back(i00);
            mIndices.push_back(i01);
            mIndices.push_back(i11);

            mIndices.push_back(i00);
            mIndices.push_back(i11);
            mIndices.push_back(i10);

            auto p00 = mVertices[i00].position;
            auto p01 = mVertices[i01].position;
            auto p10 = mVertices[i10].position;
            auto p11 = mVertices[i11].position;

            auto n0 = cross(p01 - p00, p11 - p00);
            auto n1 = cross(p11 - p00, p10 - p00);

            auto n = n0 + n1;

            mVertices[i00].normal += n;
            mVertices[i01].normal += n;
            mVertices[i10].normal += n;
            mVertices[i11].normal += n;

            auto tx0 = p10 - p00;
            auto tx1 = p11 - p01;

            auto by0 = p01 - p00;
            auto by1 = p11 - p10;

            mVertices[i00].tangent += tx0;
            mVertices[i10].tangent += tx0;
            mVertices[i01].tangent += tx1;
            mVertices[i11].tangent += tx1;

            mVertices[i00].bitangent += -by0;
            mVertices[i10].bitangent += -by1;
            mVertices[i01].bitangent += -by0;
            mVertices[i11].bitangent += -by1;
        }

    for (auto& v : mVertices)
    {
        v.normal = normalize(v.normal);
        v.tangent = normalize(v.tangent);
        v.bitangent = normalize(v.bitangent);
    }

    // ambient occlusion
    for (auto y = 0; y <= Size; ++y)
        for (auto x = 0; x <= Size; ++x)
        {
            auto& v = mVertices[vertexIdxOf(x, y)];

            auto p = v.position;
            auto n = v.normal;

            auto p0 = mVertices[vertexIdxOf(x + 1, y)].position;
            auto p1 = mVertices[vertexIdxOf(x - 1, y)].position;
            auto p2 = mVertices[vertexIdxOf(x, y + 1)].position;
            auto p3 = mVertices[vertexIdxOf(x, y - 1)].position;

            auto t0 = normalize(p0 - p);
            auto t1 = normalize(p1 - p);
            auto t2 = normalize(p2 - p);
            auto t3 = normalize(p3 - p);

            float ao = 0.0f;

            ao += glm::clamp(1 - dot(n, t0), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t1), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t2), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t3), 0.0f, 1.0f);

            v.ao = ao / 4.0f;
        }

    mVertexBuffer->bind().setData(mVertices);
    mIndexBuffer->bind().setIndices(mIndices);
}
