#pragma once

#include <glm/glm.hpp>

#include <glow-extras/camera/GenericCamera.hh>

class Terrain;

class Character
{
    // settings
public:
    float mMoveSpeed = 5.0f;
    float mLookSpeed = 1 / 200.0f;
    float mTurboFactor = 15.0f;

    float mEyeHeight = 1.70f;

private:
    glm::vec3 mPosition = {0, 30, 0};

    float mAngleAltitude = 0.0f;
    float mAngleAzimuth = 0.0f;

    float mLastMouseX = 0;
    float mLastMouseY = 0;

    bool mPressedLeft = false;
    bool mPressedRight = false;
    bool mPressedForward = false;
    bool mPressedBackward = false;
    bool mPressedShift = false;

public:
    void update(float elapsedSeconds, glow::camera::GenericCamera& cam, Terrain const& terrain);

    bool onKey(int key, int scancode, int action, int mods);
    bool onMousePosition(double x, double y);
};
