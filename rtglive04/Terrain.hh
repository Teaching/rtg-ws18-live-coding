#pragma once

#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/camera/GenericCamera.hh>

#include <glm/glm.hpp>

#include "vertex.hh"

class Terrain
{
    // settings
public:
    bool mWireframe = false;

    float mAmplitude = 40.0f;
    float mScaleFactor = 0.5f;
    int mSeed = 12345;

    int mCountX = 3;
    int mCountZ = 3;

    static constexpr int SizeExponent = 8;
    static constexpr int Size = 1 << SizeExponent;
    static constexpr int SizeMask = Size - 1;

    // data
private:
    std::vector<float> mHeights;

    std::vector<vertex> mVertices;
    std::vector<int> mIndices;

    glow::SharedArrayBuffer mVertexBuffer;
    glow::SharedElementArrayBuffer mIndexBuffer;

    int numberOfGridPoints() const { return Size * Size; }
    int idxOf(int x, int y) const { return (y & SizeMask) * Size + (x & SizeMask); }
    int vertexIdxOf(int x, int y) const { return y * (Size + 1) + x; }
    glm::vec3 posOf(int x, int y) const { return glm::vec3(x - Size / 2, mHeights[idxOf(x, y)], y - Size / 2); }


    float& heightAt(int x, int y) { return mHeights[idxOf(x, y)]; }
    float heightAt(int x, int y) const { return mHeights[idxOf(x, y)]; }

public:
    float sampleHeightAtWorldPos(glm::vec3 p) const
    {
        auto x = p.x + Size / 2;
        auto y = p.z + Size / 2;

        auto ix = glm::floor(x);
        auto iy = glm::floor(y);

        auto fx = x - ix;
        auto fy = y - iy;

        auto h00 = heightAt(ix + 0, iy + 0);
        auto h10 = heightAt(ix + 1, iy + 0);
        auto h01 = heightAt(ix + 0, iy + 1);
        auto h11 = heightAt(ix + 1, iy + 1);

        auto h0 = glm::mix(h00, h10, fx);
        auto h1 = glm::mix(h01, h11, fx);

        return glm::mix(h0, h1, fy);
    }

public:
    void init();

    void rebuild();

    void render(glow::camera::GenericCamera const& cam);

private:
    void initOpenGL();
    void generateHeightmap();
    void buildMesh();

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mMesh;

    glow::SharedTexture2D mTextureAlbedo;
    glow::SharedTexture2D mTextureAO;
    glow::SharedTexture2D mTextureHeight;
    glow::SharedTexture2D mTextureNormal;
    glow::SharedTexture2D mTextureRoughness;
};
