#pragma once

#include <glm/glm.hpp>

struct vertex
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;
    glm::vec3 bitangent;
    float ao;
};
