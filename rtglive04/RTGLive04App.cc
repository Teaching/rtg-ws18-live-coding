#include "RTGLive04App.hh"

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/data/TextureData.hh>

#include <glow-extras/geometry/Quad.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

#include <GLFW/glfw3.h>

#include "helper.hh"

using namespace glow;

void RTGLive04App::rebuildTerrain()
{
    mTerrain->rebuild();
}

bool RTGLive04App::onKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
        switch (key)
        {
        case GLFW_KEY_R:
            rebuildTerrain();
            return true;

        case GLFW_KEY_C:
            mEnableCharacter = !mEnableCharacter;
            return true;
        }

    if (mEnableCharacter)
        if (mCharacter.onKey(key, scancode, action, mods))
            return true;

    return GlfwApp::onKey(key, scancode, action, mods);
}

bool RTGLive04App::onMousePosition(double x, double y)
{
    if (mEnableCharacter)
        if (mCharacter.onMousePosition(x, y))
            return true;

    return GlfwApp::onMousePosition(x, y);
}

void RTGLive04App::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    for (auto const& target : mTargets)
        target->bind().resize(w, h);
}

void RTGLive04App::init()
{
    GlfwApp::init();

    mTerrain.reset(new Terrain());

    glow::DefaultShaderParser::addIncludePath(FILE("shader"));

    setTitle("RTG Live Coding");

    // bg
    {
        auto basepath = FILE("../textures/beach/");
        mQuad = geometry::Quad<>().generate();
        mShaderBG = Program::createFromFile("pp.bg");
        mSkybox = TextureCubeMap::createFromData(                  //
            TextureData::createFromFileCube(basepath + "posx.jpg", //
                                            basepath + "negx.jpg", //
                                            basepath + "posy.jpg", //
                                            basepath + "negy.jpg", //
                                            basepath + "posz.jpg", //
                                            basepath + "negz.jpg", //
                                            ColorSpace::sRGB));
    }

    // pipeline
    {
        mShaderOutput = Program::createFromFile("pp.out");
        mTargets.push_back(mTargetColor = TextureRectangle::create(1, 1, GL_RGB16F));
        mTargets.push_back(mTargetDepth = TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32F));

        mFramebufferScene = Framebuffer::create("fColor", mTargetColor, mTargetDepth);
    }

    mTerrain->init();

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Terrain");
        nanoForm()->addButton("[R]ebuild", [this] { rebuildTerrain(); });
        nanoForm()->addVariable("Wireframe", mTerrain->mWireframe);
        nanoForm()->addVariable("Amplitude", mTerrain->mAmplitude);
        nanoForm()->addVariable("Scale Factor", mTerrain->mScaleFactor);
        nanoForm()->addVariable("Seed", mTerrain->mSeed);
        nanoForm()->addVariable("# X", mTerrain->mCountX);
        nanoForm()->addVariable("# Z", mTerrain->mCountZ);
    }
}

void RTGLive04App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    setCursorMode(mEnableCharacter ? glfw::CursorMode::Disabled : glfw::CursorMode::Normal);

    if (mEnableCharacter)
        mCharacter.update(elapsedSeconds, *getCamera(), *mTerrain);

    // update ui
    nanoForm()->refresh();
}

void RTGLive04App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    auto cam = getCamera();

    // render scene
    {
        auto fb = mFramebufferScene->bind();

        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);

        // clear
        GLOW_SCOPED(clearColor, glm::vec3(1, 1, 1));
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // bg
        {
            GLOW_SCOPED(disable, GL_DEPTH_TEST);
            GLOW_SCOPED(disable, GL_CULL_FACE);

            auto shader = mShaderBG->use();
            shader.setTexture("uSkybox", mSkybox);
            shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
            shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
            mQuad->bind().draw();
        }

        mTerrain->render(*cam);
    }

    // write to screen
    {
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = mShaderOutput->use();
        shader.setTexture("uTexture", mTargetColor);
        mQuad->bind().draw();
    }
}
