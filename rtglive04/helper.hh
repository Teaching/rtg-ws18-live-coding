#pragma once

#include <glow/common/str_utils.hh>

#define FILE(name) util::pathOf(__FILE__) + "/" + name
