#include <glm/glm.hpp>

#include <glow/common/log.hh>
#include <glow/common/str_utils.hh>
#include <glow/gl.hh>
#include <glow/glow.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>

#include <GLFW/glfw3.h>

namespace
{
struct vertex
{
    glm::vec2 pos;
    glm::vec3 color;
};

std::vector<vertex> vertex_data;

bool left_pressed = false;

void add_triangle(vertex v0, vertex v1, vertex v2)
{
    vertex_data.push_back(v0);
    vertex_data.push_back(v1);
    vertex_data.push_back(v2);
}

void add_quad(glm::vec2 c, float s, glm::vec3 col)
{
    auto v00 = c + glm::vec2(-s, -s);
    auto v01 = c + glm::vec2(-s, +s);
    auto v10 = c + glm::vec2(+s, -s);
    auto v11 = c + glm::vec2(+s, +s);

    add_triangle({v00, col}, {v01, col}, {v11, col});
    add_triangle({v00, col}, {v11, col}, {v10, col});
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
        left_pressed = true;
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
        left_pressed = false;
}

glow::SharedArrayBuffer abVertices;
glow::SharedProgram program;
glow::SharedVertexArray mesh;

void upload_data()
{
    abVertices->bind().setData(vertex_data);
}


void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (left_pressed)
    {
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        auto x = xpos / width * 2 - 1;
        auto y = (1 - ypos / height) * 2 - 1;
        add_quad({x, y}, 0.01, {1, 1, 1});
        upload_data();
    }
}

void error_callback(int error, const char* description)
{
    glow::error() << "[GLFW Error] " << description;
}
}

int main(int argc, char** argv)
{
    if (!glfwInit())
        return EXIT_FAILURE;
    glfwSetErrorCallback(error_callback);

    auto window = glfwCreateWindow(640, 480, "RTG Live Coding", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // enable v-sync

    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, cursor_pos_callback);

    glow::initGLOW();

    program = glow::Program::createFromFile(glow::util::pathOf(__FILE__) + "/mesh");

    add_triangle({glm::vec2(-.8, -.8), glm::vec3(1, 0, 0)}, //
                 {glm::vec2(+.8, -.8), glm::vec3(0, 1, 0)}, //
                 {glm::vec2(0, +.8), glm::vec3(0, 0, 1)});

    abVertices = glow::ArrayBuffer::create();
    abVertices->defineAttribute(&vertex::pos, "aPosition");
    abVertices->defineAttribute(&vertex::color, "aColor");
    upload_data();

    mesh = glow::VertexArray::create(abVertices);

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        auto time = glfwGetTime();

        // Keep running
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT);

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);

        {
            auto shader = program->use();

            auto va = mesh->bind();
            va.draw();

            // mesh->bind().draw();
        }

        glfwSwapBuffers(window);
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
