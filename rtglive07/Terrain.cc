#include "Terrain.hh"

#include <random>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include "helper.hh"
#include "vertex.hh"

using namespace glow;

void Terrain::init()
{
    initOpenGL();
}

void Terrain::rebuild()
{
    generateHeightmap();

    buildMesh();
}

void Terrain::render(const glow::camera::GenericCamera& cam, Scene const& scene)
{
    GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

    auto pos = cam.getPosition();
    auto ox = glm::round(pos.x / Size) * Size;
    auto oz = glm::round(pos.z / Size) * Size;

    auto shader = mShader->use();

    shader.setUniform("uProj", cam.getProjectionMatrix());
    shader.setUniform("uView", cam.getViewMatrix());
    shader.setUniform("uCountX", mCountX);
    shader.setUniform("uCountZ", mCountZ);
    shader.setUniform("uTerrainSize", (float)Size);
    shader.setUniform("uTerrainOffset", glm::vec3(ox, 0, oz));

    shader.setTexture("uTexture0Albedo", mTexture0Albedo);
    shader.setTexture("uTexture0AO", mTexture0AO);
    shader.setTexture("uTexture0Height", mTexture0Height);
    shader.setTexture("uTexture0Normal", mTexture0Normal);
    shader.setTexture("uTexture0Roughness", mTexture0Roughness);

    shader.setTexture("uTexture1Albedo", mTexture1Albedo);
    shader.setTexture("uTexture1AO", mTexture1AO);
    shader.setTexture("uTexture1Height", mTexture1Height);
    shader.setTexture("uTexture1Normal", mTexture1Normal);
    shader.setTexture("uTexture1Roughness", mTexture1Roughness);

    shader.setTexture("uTexture2Albedo", mTexture2Albedo);
    shader.setTexture("uTexture2AO", mTexture2AO);
    shader.setTexture("uTexture2Height", mTexture2Height);
    shader.setTexture("uTexture2Normal", mTexture2Normal);
    shader.setTexture("uTexture2Roughness", mTexture2Roughness);

    mMesh->bind().draw(mCountX * mCountZ);
}

void Terrain::initOpenGL()
{
    // load texture
    mTexture0Albedo
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_albedo.png"), ColorSpace::Linear);
    mTexture0AO = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_ao.png"), ColorSpace::Linear);
    mTexture0Height
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_height.png"), ColorSpace::Linear);
    mTexture0Normal
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_normal.png"), ColorSpace::Linear);
    mTexture0Roughness
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_Rock_CliffMountain2_1K_roughness.png"), ColorSpace::Linear);

    mTexture1Albedo
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_RiverStones_2x2_1024_albedo.png"), ColorSpace::sRGB);
    mTexture1AO = Texture2D::createFromFile(FILE("../textures/TexturesCom_RiverStones_2x2_1024_ao.png"), ColorSpace::Linear);
    mTexture1Height
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_RiverStones_2x2_1024_height.png"), ColorSpace::Linear);
    mTexture1Normal
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_RiverStones_2x2_1024_normal.png"), ColorSpace::Linear);
    mTexture1Roughness
        = Texture2D::createFromFile(FILE("../textures/TexturesCom_RiverStones_2x2_1024_roughness.png"), ColorSpace::Linear);

    mTexture2Albedo = Texture2D::createFromFile(FILE("../textures/brown_mud_leaves_01_diff_1k.png"), ColorSpace::sRGB);
    mTexture2AO = Texture2D::createFromFile(FILE("../textures/brown_mud_leaves_01_AO_1k.png"), ColorSpace::Linear);
    mTexture2Height = Texture2D::createFromFile(FILE("../textures/brown_mud_leaves_01_disp_1k.png"), ColorSpace::Linear);
    mTexture2Normal = Texture2D::createFromFile(FILE("../textures/brown_mud_leaves_01_Nor_1k.png"), ColorSpace::Linear);
    mTexture2Roughness = Texture2D::createFromFile(FILE("../textures/brown_mud_leaves_01_rough_1k.png"), ColorSpace::Linear);

    // load shader
    mShader = Program::createFromFile("mesh");

    mVertexBuffer = ArrayBuffer::create({{&vertex::position, "aPosition"},   //
                                         {&vertex::tangent, "aTangent"},     //
                                         {&vertex::bitangent, "aBitangent"}, //
                                         {&vertex::splat, "aSplat"},         //
                                         {&vertex::ao, "aAO"}});

    mIndexBuffer = ElementArrayBuffer::create();

    mMesh = VertexArray::create(mVertexBuffer, mIndexBuffer, GL_TRIANGLES);
}

void Terrain::generateHeightmap()
{
    mHeights.clear();
    mHeights.resize(numberOfGridPoints());

    std::default_random_engine rng;
    rng.seed(mSeed);

    mHeights[0] = 0.0f;

    auto s = Size / 2;
    auto scale = mAmplitude;
    while (s >= 1)
    {
        std::normal_distribution<float> noise(0.0f, scale);

        // diamond step
        for (auto y = s; y < Size; y += s * 2)
            for (auto x = s; x < Size; x += s * 2)
            {
                auto h00 = heightAt(x - s, y - s);
                auto h10 = heightAt(x + s, y - s);
                auto h01 = heightAt(x - s, y + s);
                auto h11 = heightAt(x + s, y + s);

                auto h = (h00 + h10 + h01 + h11) / 4;
                h += noise(rng);

                heightAt(x, y) = h;
            }

        // square step
        for (auto y = 0; y < Size; y += s)
            for (auto ix = s; ix < Size; ix += s * 2)
            {
                auto x = ix - (y / s) % 2 * s;

                auto h0 = heightAt(x - s, y);
                auto h1 = heightAt(x + s, y);
                auto h2 = heightAt(x, y - s);
                auto h3 = heightAt(x, y + s);

                auto h = (h0 + h1 + h2 + h3) / 4;
                h += noise(rng);

                heightAt(x, y) = h;
            }

        // advance
        s /= 2;
        scale *= mScaleFactor;
    }
}

void Terrain::buildMesh()
{
    mVertices.clear();
    mIndices.clear();

    mVertices.resize((Size + 1) * (Size + 1));
    for (auto y = 0; y <= Size; ++y)
        for (auto x = 0; x <= Size; ++x)
        {
            auto& v = mVertices[vertexIdxOf(x, y)];

            v.position = posOf(x, y);

            auto px0 = posOf(x - 1, y);
            auto px1 = posOf(x + 1, y);
            auto py0 = posOf(x, y - 1);
            auto py1 = posOf(x, y + 1);

            v.tangent = normalize(px1 - px0);
            v.bitangent = normalize(py0 - py1);
            auto n = cross(v.tangent, v.bitangent);

            auto t0 = normalize(px0 - v.position);
            auto t1 = normalize(px1 - v.position);
            auto t2 = normalize(py0 - v.position);
            auto t3 = normalize(py1 - v.position);

            float ao = 0.0f;

            ao += glm::clamp(1 - dot(n, t0), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t1), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t2), 0.0f, 1.0f);
            ao += glm::clamp(1 - dot(n, t3), 0.0f, 1.0f);

            v.ao = ao / 4.0f;

            // Splat mapping
            float slope = glm::smoothstep(1.0f, 0.5f, n.y);
            float mud_part = glm::smoothstep(-5.0f, 5.0f, v.position.y);
            glm::vec3 splat;
            splat.x = slope;
            splat.y = (1 - slope) * (1 - mud_part);
            splat.z = (1 - slope) * mud_part;
            v.splat = splat;
        }

    for (auto y = 0; y < Size; ++y)
        for (auto x = 0; x < Size; ++x)
        {
            // 00 --- 10
            //  |      |
            // 01 --- 11
            auto i00 = vertexIdxOf(x + 0, y + 0);
            auto i01 = vertexIdxOf(x + 0, y + 1);
            auto i10 = vertexIdxOf(x + 1, y + 0);
            auto i11 = vertexIdxOf(x + 1, y + 1);

            mIndices.push_back(i00);
            mIndices.push_back(i01);
            mIndices.push_back(i11);

            mIndices.push_back(i00);
            mIndices.push_back(i11);
            mIndices.push_back(i10);
        }

    mVertexBuffer->bind().setData(mVertices);
    mIndexBuffer->bind().setIndices(mIndices);
}
