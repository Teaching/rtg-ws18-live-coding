uniform mat4 uProj;
uniform mat4 uView;
uniform mat4 uModel;

in vec3 aPosition;

out vec3 vWorldPosition;
out vec3 vNormal;

void main()
{
    vWorldPosition = vec3(uModel * vec4(aPosition, 1));
    vNormal = aPosition;

    gl_Position = uProj * uView * vec4(vWorldPosition, 1);
}
