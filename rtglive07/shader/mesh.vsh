uniform mat4 uProj;
uniform mat4 uView;

uniform int uCountX;
uniform int uCountZ;
uniform float uTerrainSize;
uniform vec3 uTerrainOffset;

in vec3 aPosition;
in vec3 aTangent;
in vec3 aBitangent;
in vec3 aSplat;
in float aAO;

out vec3 vPosition;
out vec3 vTangent;
out vec3 vBitangent;
out vec3 vSplat;
out float vAO;

void main()
{
    int id = gl_InstanceID;
    int x = id % uCountX - uCountX / 2;
    int z = id / uCountX - uCountZ / 2;
    vec3 offset = vec3(x, 0, z) * uTerrainSize + uTerrainOffset;

    vSplat = aSplat;
    vPosition = aPosition + offset;
    vTangent = aTangent;
    vBitangent = aBitangent;
    vAO = aAO;

    gl_Position = uProj * uView * vec4(vPosition, 1);
}
