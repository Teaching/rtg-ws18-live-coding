#pragma once

#include <queue>

#include <glm/glm.hpp>

#include <glow-extras/colors/color.hh>
#include <glow-extras/colors/material_colors.hh>

enum class particle_type
{
    rainbow,
    explosion
};

struct environment
{
    float gravity = 9.81f * 100;

    float bounciness = 0.7f;

    float width;
    float height;
};

struct collision_event
{
    glm::vec2 position;
    glm::vec2 velocity;
    glm::vec2 normal;
    particle_type type;
};

class ParticleEventQueue
{
private:
    std::queue<collision_event> mEvents;

public:
    void onCollision(collision_event e) { mEvents.push(e); }
    bool process(collision_event& e)
    {
        if (mEvents.empty())
            return false;

        e = mEvents.front();
        mEvents.pop();
        return true;
    }
};

struct particle
{
    glm::vec2 position;
    glm::vec2 velocity;
    particle_type type;
    float timeLeft = 2.0f;
    float timeElapsed = 0.0f;
    float size;
    float random;
    glm::vec3 color;

    bool is_dead() const { return timeLeft < 0; }

    void update(float dt, environment const& env, ParticleEventQueue& events)
    {
        using namespace glow::colors;

        // particle motion equation
        auto accel = glm::vec2(0, -env.gravity);
        velocity += dt * accel;
        position += dt * velocity;

        timeLeft -= dt;
        timeElapsed += dt;

        switch (type)
        {
        case particle_type::rainbow:
            size = 15;
            size *= glm::smoothstep(0.0f, 0.5f, timeElapsed); // fade-in
            size *= glm::smoothstep(0.0f, 0.5f, timeLeft);    // fade-out

            color = color::from_hsv(360 * timeElapsed / (timeElapsed + timeLeft), 1, 1).to_rgb();
            break;
        case particle_type::explosion:
            size = 3 + 3 * random;
            color = mix(material::red(), material::yellow(), random).to_rgb();
            break;
        }

        // collision
        if (position.y < size && velocity.y < 0)
        {
            events.onCollision({position, velocity, glm::vec2(0, 1), type});
            velocity.y = -velocity.y * env.bounciness;
        }
        if (position.x < size && velocity.x < 0)
        {
            events.onCollision({position, velocity, glm::vec2(1, 0), type});
            velocity.x = -velocity.x * env.bounciness;
        }
        if (position.x > env.width - size && velocity.x > 0)
        {
            events.onCollision({position, velocity, glm::vec2(-1, 0), type});
            velocity.x = -velocity.x * env.bounciness;
        }
    }
};
