uniform vec2 uWindowSize;

in vec2 aPosition;
in vec3 aColor;

out vec3 vColor;

void main()
{
    vColor = aColor;

    gl_Position = vec4(aPosition / uWindowSize * 2 - 1, 0, 1);
}
