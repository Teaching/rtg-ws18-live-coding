#include "RTGLive01App.hh"

#include <glm/ext.hpp>

#include <glow/common/str_utils.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>

#include <nanogui/nanogui.h>

using namespace glow;

void RTGLive01App::updateParticleMesh()
{
    mVertices.clear();

    for (auto const& p : mParticles)
    {
        addQuad(p.position, p.size, p.color);
    }

    mParticleAB->bind().setData(mVertices);
}

void RTGLive01App::spawnRandomRainbowParticle()
{
    std::uniform_real_distribution<> pos_x(0.0f, getWindowWidth());
    std::uniform_real_distribution<> pos_y(0.0f, getWindowHeight());
    std::uniform_real_distribution<> dis01(0.0f, 1.0f);

    particle p;
    p.type = particle_type::rainbow;
    p.timeLeft = mLifetime;
    p.position.x = pos_x(mRandom);
    p.position.y = pos_y(mRandom);
    p.random = dis01(mRandom);
    mParticles.push_back(p);
}

void RTGLive01App::onParticleCollision(const collision_event& e)
{
    if (e.type != particle_type::rainbow)
        return;

    std::uniform_real_distribution<> dis_timeleft(0.5f, 1.5f);
    std::uniform_real_distribution<> dis_v(-1.0f, 1.0f);
    std::uniform_real_distribution<> dis01(0.0f, 1.0f);

    int pCnt = length(e.velocity) / 5 - 5;

    for (auto i = 0; i < pCnt; ++i)
    {
        particle p;
        p.type = particle_type::explosion;
        p.position = e.position;
        p.timeLeft = dis_timeleft(mRandom);
        do
        {
            p.velocity.x = dis_v(mRandom);
            p.velocity.y = dis_v(mRandom);
        } while (length(p.velocity) > 1);
        p.velocity *= length(e.velocity);
        p.random = dis01(mRandom);
        mParticles.push_back(p);
    }
}

void RTGLive01App::init()
{
    GlfwApp::init();

    setTitle("RTG Live Coding");

    // particle init
    {
        mParticleAB = ArrayBuffer::create({
            {&vertex::pos, "aPosition"}, //
            {&vertex::color, "aColor"},  //
        });
        mParticleVA = VertexArray::create(mParticleAB);
        mParticleShader = Program::createFromFile(glow::util::pathOf(__FILE__) + "/particle");
    }

    // ui init
    {
        nanoForm()->addWindow({10, 10}, "RTG Live Coding");

        nanoForm()->addGroup("Simulation");
        nanoForm()->addVariable("Simulate", mSimulate);

        nanoForm()->addGroup("Particles");
        nanoForm()->addVariable("# Particle", mParticleCount, false);
        nanoForm()->addVariable("Spawn Interval", mSpawnInterval);
        nanoForm()->addVariable("Gravity", mEnvironment.gravity);
        nanoForm()->addVariable("Bounciness", mEnvironment.bounciness);
        nanoForm()->addVariable("Lifetime", mLifetime);
        nanoForm()->addButton("Spawn Random", [this] { spawnRandomRainbowParticle(); });
    }
}

void RTGLive01App::update(float elapsedSeconds)
{
    GlfwApp::update(elapsedSeconds);

    mEnvironment.width = getWindowWidth();
    mEnvironment.height = getWindowHeight();

    // update particles
    if (mSimulate)
    {
        // motion update
        for (auto& p : mParticles)
            p.update(elapsedSeconds, mEnvironment, mParticleEvents);

        // spawning
        mSpawnAccum -= elapsedSeconds;
        while (mSpawnAccum < 0)
        {
            mSpawnAccum += mSpawnInterval;
            spawnRandomRainbowParticle();
        }

        // collision
        {
            collision_event e;
            while (mParticleEvents.process(e))
                onParticleCollision(e);
        }
    }

    // removing particles
    for (auto i = 0u; i < mParticles.size(); ++i)
        if (mParticles[i].is_dead())
        {
            std::swap(mParticles[i], mParticles.back());
            mParticles.pop_back();
        }

    mParticleCount = mParticles.size();

    // update ui
    nanoForm()->refresh();
}

void RTGLive01App::render(float elapsedSeconds)
{
    GlfwApp::render(elapsedSeconds);

    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    GLOW_SCOPED(clearColor, glm::vec3(1, 1, 1));
    glClear(GL_COLOR_BUFFER_BIT);

    // render particle
    if (mParticleCount > 0)
    {
        updateParticleMesh();

        auto shader = mParticleShader->use();
        shader.setUniform("uWindowSize", glm::vec2(getWindowWidth(), getWindowHeight()));
        mParticleVA->bind().draw();
    }
}
