#pragma once

#include <random>
#include <vector>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include "vertex.hh"
#include "particle.hh"

class RTGLive01App : public glow::glfw::GlfwApp
{
    // logic
private:
    std::default_random_engine mRandom;
    bool mSimulate = true;
    environment mEnvironment;
    std::vector<particle> mParticles;
    float mLifetime = 3.0f;
    int mParticleCount = 0;

    ParticleEventQueue mParticleEvents;

    float mSpawnInterval = 0.05f;
    float mSpawnAccum = 0.0f;

    void updateParticleMesh();
    void spawnRandomRainbowParticle();
    void onParticleCollision(collision_event const& e);

    // rendering
private:
    glow::SharedArrayBuffer mParticleAB;
    glow::SharedVertexArray mParticleVA;
    glow::SharedProgram mParticleShader;

    std::vector<vertex> mVertices;

    void addTriangle(vertex v0, vertex v1, vertex v2)
    {
        mVertices.push_back(v0);
        mVertices.push_back(v1);
        mVertices.push_back(v2);
    }

    void addQuad(glm::vec2 c, float s, glm::vec3 col)
    {
        auto v00 = c + glm::vec2(-s, -s);
        auto v01 = c + glm::vec2(-s, +s);
        auto v10 = c + glm::vec2(+s, -s);
        auto v11 = c + glm::vec2(+s, +s);

        addTriangle({v00, col}, {v01, col}, {v11, col});
        addTriangle({v00, col}, {v11, col}, {v10, col});
    }

public:
    /// Called once GLOW is initialized. Allocated your resources and init your logic here.
    virtual void init() override;
    /// Called with at 1 / getUpdateRate() Hz (timestep)
    virtual void update(float elapsedSeconds) override;
    /// Called as fast as possible for rendering (elapsedSeconds is not fixed here)
    virtual void render(float elapsedSeconds) override;
};
