#pragma once

#include <glm/glm.hpp>

struct vertex
{
    glm::vec2 pos;
    glm::vec3 color;
};